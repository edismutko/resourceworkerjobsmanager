﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResourceWorkerJobsManager
{
    public interface IJobsManager
    {
        IJob TakeNextJob();
        void AddJob(IJob job);
    }
}