﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResourceWorkerJobsManager
{
    public interface IJob
    {
        int ID { get; set; }
        Action Action { get; set; }
        TimeSpan Duration { get; set; }
        bool HighPriority { get; set; }
    }
}