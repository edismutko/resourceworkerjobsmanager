﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ResourceWorkerJobsManager
{
    public class ResourceWorkerJobsManager
    {
        private IJobsManager jobsManager;
        private IWorkersManager workersManager;
        private bool active;

        public ResourceWorkerJobsManager(IJobsManager jobsManager, IWorkersManager workersManager)
        {
            this.jobsManager = jobsManager;
            this.workersManager = workersManager;
            this.active = false;
        }

        public void Start()
        {
            this.active = true;
            Assign();
        }

        private void Assign()
        {
            Task.Run(() => {
                while (this.active)
                {
                    IJob job = jobsManager.TakeNextJob();
                    IWorker worker;
                    if (workersManager.TryGetAvailableWorkerByTime(job.Duration, out worker))
                    {
                        // execute the process
                    }
                    else
                    {
                        job.HighPriority = true;
                        jobsManager.AddJob(job);
                    }
                }
            });
        }

        public void Stop()
        {
            this.active = false;
        }
    }
}