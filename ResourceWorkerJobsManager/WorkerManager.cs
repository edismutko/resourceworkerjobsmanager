﻿using System;
using System.Collections.Concurrent;
using System.Linq;

namespace ResourceWorkerJobsManager
{
    public class WorkerManager : IWorkersManager
    {
        private object lockObj = new object();
        private BlockingCollection<IWorker> availableWorkers;

        public WorkerManager()
        {
            this.availableWorkers = new BlockingCollection<IWorker>(new ConcurrentBag<IWorker>());
        }

        public void AddWorker(IWorker worker)
        {
            // the lock here is needed because the use of the bag collection which allows duplicates
            // and in order to prevent this we need check if contains.
            lock (lockObj)
            {
                if (!this.availableWorkers.Contains(worker))
                {
                    this.availableWorkers.Add(worker);
                }
            }
        }

        public bool TryGetAvailableWorkerByTime(TimeSpan handleTime, out IWorker worker)
        {
            worker = null;

            lock (lockObj)
            {
                // the foreach loop runs on a snapshot so if an item is added during the foreach it will not be counted
                foreach (IWorker availableWorker in this.availableWorkers)
                {
                    if (handleTime < availableWorker.MaxHandleTime)
                    {
                        if ((worker == null) || (worker.MaxHandleTime > availableWorker.MaxHandleTime))
                            worker = availableWorker;
                    }
                }

                RemoveWorker(worker);
            }

            if (worker == null) return false;
            else return true;
        }

        public void RemoveWorker(IWorker worker)
        {
            // always should be one worker only
            this.availableWorkers.TakeWhile((w) => { return w == worker; });
        }
    }
}