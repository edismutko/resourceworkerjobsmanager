﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResourceWorkerJobsManager
{
    public interface IWorkersManager
    {
        bool TryGetAvailableWorkerByTime(TimeSpan handleTime, out IWorker worker);
        void AddWorker(IWorker worker);
        void RemoveWorker(IWorker worker);
    }
}