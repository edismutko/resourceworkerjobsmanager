﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResourceWorkerJobsManager
{
    public interface IWorker
    {
        TimeSpan MaxHandleTime { get; set; }

        void DoWork(Action a, JobDuration t);
    }
}