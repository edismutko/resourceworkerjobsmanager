﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResourceWorkerJobsManager
{
    public class JobDuration
    {
        public TimeSpan Duration
        {
            get;
            private set;
        }

        public JobDuration(TimeSpan duration)
        {
            this.Duration = duration;
        }
    }
}