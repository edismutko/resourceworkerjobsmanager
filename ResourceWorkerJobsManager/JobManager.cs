﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace ResourceWorkerJobsManager
{
    public class JobManager : IJobsManager
    {
        private object lockObj = new object();
        // by default the collection uses concurrent queue
        BlockingCollection<IJob> jobs;
        BlockingCollection<IJob> priorityJobs;

        public JobManager()
        {
            jobs = new BlockingCollection<IJob>();
            priorityJobs = new BlockingCollection<IJob>();
        }

        public void AddJob(IJob job)
        {
            lock (lockObj)
            {
                if (job.HighPriority) priorityJobs.Add(job);
                else jobs.Add(job);
            }
        }

        public IJob TakeNextJob()
        {
            lock (lockObj)
            {
                // does it crash if the blocking collection is empty ?
                if (priorityJobs.Count > 0) return priorityJobs.Take();
                else return jobs.Take();
            }
        }
    }
}